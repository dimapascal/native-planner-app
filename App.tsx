/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import 'react-native-gesture-handler';

import { DefaultTheme, Provider as PaperProvider } from 'react-native-paper';
import React, { Component } from 'react';

import { Colors } from './util/colors';
import { PersistGate } from 'redux-persist/integration/react';
import { Provider } from 'react-redux';
import { RootNavigator } from './src/Navigator/Root.Navigator';
import configureStore from './src/Redux/Root.Reducer';

const {store, persistor} = configureStore();

const theme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    ...Colors,
  },
};

export default class App extends Component {
  render() {
    return (
      <PaperProvider theme={theme}>
        <Provider store={store}>
          <PersistGate loading={null} persistor={persistor}>
            <RootNavigator />
          </PersistGate>
        </Provider>
      </PaperProvider>
    );
  }
}
