import { StyleSheet } from 'react-native';

const Flex = StyleSheet.create({
  box: {
    flex: 1,
  },
  grow: {
    flexGrow: 1,
  },
  wrap: {
    flexWrap: 'wrap',
  },
  direction_column: {
    flexDirection: 'column',
  },
  direction__row: {
    flexDirection: 'row',
  },
  direction__column_reverse: {
    flexDirection: 'column-reverse',
  },
  align_items__start: {
    alignItems: 'flex-start',
  },
  align_items__end: {
    alignItems: 'flex-end',
  },
  align_items__center: {
    alignItems: 'center',
  },
  align_items_stretch: {
    alignItems: 'stretch',
  },
  align_items__baseline: {
    alignItems: 'baseline',
  },
  align_self__auto: {
    alignSelf: 'auto',
  },
  align_self__start: {
    alignSelf: 'flex-start',
  },
  align_self__end: {
    alignSelf: 'flex-end',
  },
  align_self__center: {
    alignSelf: 'center',
  },
  align_self__stretch: {
    alignSelf: 'stretch',
  },
  align_self__baseline: {
    alignSelf: 'baseline',
  },
  align_content__flexStart: {
    alignContent: 'flex-start',
  },
  align_content__start: {
    alignContent: 'flex-start',
  },
  align_content__end: {
    alignContent: 'flex-end',
  },
  align_content__center: {
    alignContent: 'center',
  },
  align_content__stretch: {
    alignContent: 'stretch',
  },
  align_content__spaceBetween: {
    alignContent: 'space-between',
  },
  align_content__spaceAround: {
    alignContent: 'space-around',
  },
  justify__start: {
    justifyContent: 'flex-start',
  },
  justify__end: {
    justifyContent: 'flex-end',
  },
  justify__center: {
    justifyContent: 'center',
  },
  justify__spaceBetween: {
    justifyContent: 'space-between',
  },
  justify__spaceAround: {
    justifyContent: 'space-around',
  },
  justify__spaceEvenly: {
    justifyContent: 'space-evenly',
  },
});

export default Flex;
