import { Dimensions, StyleSheet } from 'react-native';

const Position = StyleSheet.create({
  absolute: {
    position: 'absolute'
  },
  relative: {
    position: 'relative'
  },
  right: {
    right: 0
  },
  left: {
    left: 0
  },
  bottom: {
    bottom: 0
  },
  top: {
    top: 0
  },
  one: {
    zIndex: 1
  },
  second: {
    zIndex: 2
  },
  zero: {
    top: 0,
    bottom: 0,
    left: 0,
    right: 0
  },
  full: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height
  },
  full__width: {
    width: Dimensions.get('window').width
  },
  full__height: {
    height: Dimensions.get('window').height
  }
});

export default Position;
