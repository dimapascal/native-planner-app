import { StyleSheet } from 'react-native';

const Font = StyleSheet.create({
  center: {
    textAlign: 'center',
  },
  left: {
    textAlign: 'left',
  },
  right: {
    textAlign: 'right',
  },
  underline: {
    textDecorationLine: 'underline',
  },
  upperCase: {
    textTransform: 'uppercase',
  },
});
export default Font;
