import { StyleSheet } from 'react-native';

const Block = StyleSheet.create({
  full__width: {
    width: '100%',
  },
  full__height: {
    height: '100%',
  },
  left: {
    marginRight: 'auto',
  },
  right: {
    marginLeft: 'auto',
  },
});

export default Block;
