import Block from './Components/Block';
import Flex from './Components/Flex';
import Font from './Components/Font';
import Position from './Components/Position';

export const Styles = {
  Flex,
  Position,
  Block,
  Font,
};
