import { PixelRatio } from 'react-native';

const ratio = PixelRatio.getPixelSizeForLayoutSize(1) / PixelRatio.get();

/**
 *
 * @param {number} pixels
 */
export function getPixel(pixels: number): number {
  return pixels * ratio;
}
