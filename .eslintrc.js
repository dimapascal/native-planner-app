module.exports = {
  root: true,
  extends: '@react-native-community',
  parser: '@typescript-eslint/parser',
  plugins: ['@typescript-eslint'],
  'editor.codeActionsOnSave': {
    // For ESLint
    'source.fixAll.eslint': true,
    // For TSLint
    'source.fixAll.tslint': true,
    // For Stylelint
    'source.fixAll.stylelint': true,
  },
};
