name=$@
lowercase=$(echo "$@" | tr '[:upper:]' '[:lower:]')

cp -r ./.generator/Container ./.generator/temp/$name
mv ./.generator/temp/$name/Example.Container.tsx ./.generator/temp/$name/$name.Container.tsx
mv ./.generator/temp/$name/Example.Styles.ts ./.generator/temp/$name/$name.Styles.ts

find ./.generator/temp/$name -type f -name \*.Container.tsx -exec sed -i.bak 's|Example|'$name'|g' {} +
find ./.generator/temp/$name -type f -name \*.Container.tsx -exec sed -i.bak 's|example|'$lowercase'|g' {} +
find ./.generator/temp/$name -type f -name \*.Styles.ts -exec sed -i.bak 's|Example|'$name'|g' {} +
find ./.generator/temp/$name -type f -name \*.Styles.ts -exec sed -i.bak 's|example|'$lowercase'|g' {} +
find ./.generator/temp/$name -name \*.bak -delete

mv ./.generator/temp/$name ./src/Container/$name