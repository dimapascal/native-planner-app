import { InferProps } from 'prop-types';
import React, { Component, Dispatch } from 'react';
import { SafeAreaView, Text, View } from 'react-native';
import { connect } from 'react-redux';
import { AnyAction } from 'redux';

import IDefaultProps from '../../Interface/IDefaultProps';
import { IRootReducer } from '../../Interface/IRootReducer';
import ExampleStyles from './Example.Styles';

type Props = {};

const style = ExampleStyles;

class ExampleContainer extends Component<InferProps<Props> & IDefaultProps> {
  readonly state = {};

  render() {
    return (
      <SafeAreaView style={style.content}>
        <View>
          <Text>Example container ready</Text>
        </View>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = (_state: IRootReducer) => ({});

const mapDispatchToProps = (_dispatch: Dispatch<AnyAction>) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(ExampleContainer);
