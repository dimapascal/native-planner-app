import { AnyAction } from 'redux';

const initialState: any = [];

export enum TasksActionTypes {
  ADD_TASK = 'SET_AUTH_REDUCER_ACTION',
}

export const tasksReducer = (state = initialState, action: AnyAction) => {
  if (TasksActionTypes.ADD_TASK === action.type) {
    return [...state, action.payload];
  }
  return state;
};

function addTask(value: any): AnyAction {
  return {type: TasksActionTypes.ADD_TASK, payload: value};
}

export const TasksActions = {
  addTask,
};
