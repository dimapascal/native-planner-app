import { AnyAction } from 'redux';

const initialState: Record<string, any> = {
  username: '',
  email: '',
  password: '',
};

export enum AuthActionTypes {
  SET_AUTH = 'SET_AUTH_REDUCER_ACTION',
}

export const authReducer = (state = initialState, action: AnyAction) => {
  if (AuthActionTypes.SET_AUTH === action.type) {
    return {...state, ...action.payload};
  }
  return state;
};

function setAuth(value: any): AnyAction {
  return {type: AuthActionTypes.SET_AUTH, payload: value};
}

export const AuthActions = {
  setAuth,
};
