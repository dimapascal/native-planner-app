import { applyMiddleware, combineReducers, compose, createStore } from 'redux';
import { persistReducer, persistStore } from 'redux-persist';
import thunk from 'redux-thunk';

import AsyncStorage from '@react-native-community/async-storage';

import { authReducer } from './Auth.Reducer';
import { tasksReducer } from './Tasks.Reducer';

const rootReducer = combineReducers({
  authReducer,
  tasksReducer,
});

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  whitelist: ['authReducer'],
  blacklist: ['tasksReducer'],
};

let middleware = [thunk];

if (process.env.NODE_ENV === `development`) {
  const {logger} = require(`redux-logger`);
  middleware.push(logger);
}

export default function configureStore() {
  const enhancer = compose(applyMiddleware(...middleware));
  const persistedReducer = persistReducer(persistConfig, rootReducer);
  const store = createStore(persistedReducer, enhancer);
  const persistor = persistStore(store);
  return {store, persistor};
}
