import PropTypes, { InferProps } from 'prop-types';
import React from 'react';
import { Text, View } from 'react-native';

import WelcomeStyles from './Welcome.Styles';

const propTypes = {
  myProp: PropTypes.any,
};

const defaultProps = {
  myProp: null,
};

const style = WelcomeStyles;

/**
 * @param {object} props
 * @param {any} props.myProp
 */
function WelcomeComponent(props: InferProps<typeof propTypes>): JSX.Element {
  const {myProp} = props;

  return (
    <View style={style.content}>
      <Text>Welcome component is ready!</Text>
    </View>
  );
}

WelcomeComponent.propTypes = propTypes;
WelcomeComponent.defaultProps = defaultProps;

export default WelcomeComponent;
