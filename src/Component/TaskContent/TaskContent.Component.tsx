import PropTypes, { InferProps } from 'prop-types';
import React from 'react';
import { Text, View } from 'react-native';

import TaskContentStyles from './TaskContent.Styles';

const propTypes = {
  section: PropTypes.any,
  sections: PropTypes.array,
  index: PropTypes.number.isRequired,
  isActive: PropTypes.bool.isRequired,
};

const defaultProps = {
  section: null,
  sections: [],
};

const style = TaskContentStyles;

/**
 * @param {object} props
 * @param {any} props.section
 * @param {number} props.index
 * @param {boolean} props.isActive
 * @param {Array<any>} props.sections
 */
function TaskContentComponent(
  props: InferProps<typeof propTypes>,
): JSX.Element {
  const {section, index, isActive, sections} = props;

  return (
    <View style={style.content}>
      <Text>TaskContent component is ready!</Text>
    </View>
  );
}

TaskContentComponent.propTypes = propTypes;
TaskContentComponent.defaultProps = defaultProps;

export default TaskContentComponent;
