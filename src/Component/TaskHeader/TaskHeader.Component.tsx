import PropTypes, { InferProps } from 'prop-types';
import React from 'react';
import { Text, View } from 'react-native';
import * as Animatable from 'react-native-animatable';

import TaskHeaderStyles from './TaskHeader.Styles';

const propTypes = {
  section: PropTypes.any,
  sections: PropTypes.array,
  index: PropTypes.number.isRequired,
  isActive: PropTypes.bool.isRequired,
};

const defaultProps = {
  section: null,
  sections: [],
};

const style = TaskHeaderStyles;

/**
 * @param {object} props
 * @param {any} props.section
 * @param {number} props.index
 * @param {boolean} props.isActive
 * @param {Array<any>} props.sections
 */
function TaskHeaderComponent(props: InferProps<typeof propTypes>): JSX.Element {
  const {section, index, isActive, sections} = props;

  return (
    <Animatable.View
      duration={300}
      transition="backgroundColor"
      style={{
        backgroundColor: isActive
          ? 'rgba(255,255,255,1)'
          : 'rgba(245,252,255,1)',
      }}>
      <Text>title</Text>
    </Animatable.View>
  );
}

TaskHeaderComponent.propTypes = propTypes;
TaskHeaderComponent.defaultProps = defaultProps;

export default TaskHeaderComponent;
