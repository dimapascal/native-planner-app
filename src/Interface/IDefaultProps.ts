import {NavigationParams, NavigationScreenProp} from 'react-navigation';

import {NavigationState} from '@react-navigation/native';

export default interface IDefaultProps {
  navigation: NavigationScreenProp<NavigationState, NavigationParams>;
}
