import * as React from 'react';
import Icon from 'react-native-vector-icons/Entypo';

import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import CurrentTasksContainer from '../Container/CurrentTasks/CurrentTasks.Container';

const Tab = createBottomTabNavigator();

export default function MainBottomNavigator() {
  return (
    <Tab.Navigator tabBarOptions={{showLabel: false}}>
      <Tab.Screen
        name="CurrentTasks"
        options={{
          tabBarIcon: ({color, size}) => (
            <Icon name="clipboard" color={color} size={size}></Icon>
          ),
        }}
        component={CurrentTasksContainer}
      />
    </Tab.Navigator>
  );
}
