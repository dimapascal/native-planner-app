import * as React from 'react';

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import LoginContainer from '../Container/Login/Login.Container';
import RegisterContainer from '../Container/Register/Register.Container';
import MainBottomNavigator from './MainBottom.Navigator';

const Stack = createStackNavigator();

export function RootNavigator() {
  return (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{
          headerShown: false,
        }}
        initialRouteName="MainBottomNavigator">
        <Stack.Screen name="LoginContainer" component={LoginContainer} />
        <Stack.Screen name="RegisterContainer" component={RegisterContainer} />
        <Stack.Screen
          name="MainBottomNavigator"
          component={MainBottomNavigator}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
