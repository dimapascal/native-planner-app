import { InferProps } from 'prop-types';
import React, { Component, Dispatch } from 'react';
import { ImageBackground, SafeAreaView, Text, View } from 'react-native';
import { Button, TextInput } from 'react-native-paper';
import Icon from 'react-native-vector-icons/FontAwesome';
import { connect } from 'react-redux';
import { AnyAction } from 'redux';

import { Colors } from '../../../util/colors';
import { Images } from '../../../util/images';
import { getPixel } from '../../../util/pixel';
import IDefaultProps from '../../Interface/IDefaultProps';
import { IRootReducer } from '../../Interface/IRootReducer';
import { AuthActions } from '../../Redux/Auth.Reducer';
import RegisterStyles from './Register.Styles';

type Props = {
  setAuth: Function;
};

const style = RegisterStyles;

class RegisterContainer extends Component<InferProps<Props> & IDefaultProps> {
  readonly state = {
    username: '',
    email: '',
    password: '',
    usernameErrorMessage: '',
    emailErrorMessage: '',
    passwordErrorMessage: '',
  };

  setData(key: string, value: any = null) {
    return this.setState({[key]: value});
  }

  registerUser() {
    const {username, email, password} = this.state;
    this.props.setAuth({username, email, password});
  }

  onPressSubmit = () => {
    if (this.inputsAreValid) {
      this.registerUser();
      this.props.navigation.navigate('MainBottomNavigator');
    }
  };

  onPressSignIn = () => {
    this.props.navigation.navigate('Login');
  };
  onChangeUsername = (value: string) => {
    this.setData('usernameErrorMessage');
    this.setData('username', value);
  };
  onChangeEmail = (value: string) => {
    this.setData('emailErrorMessage');
    this.setData('email', value);
  };
  onChangePassword = (value: string) => {
    this.setData('passwordErrorMessage');
    this.setData('password', value);
  };

  get inputsAreValid() {
    const username = this.usernameIsValid;
    const email = this.emailIsValid;
    const password = this.passwordIsValid;
    if (!username) {
      this.setData('usernameErrorMessage', 'Username is to short');
    }
    if (!email) {
      this.setData('emailErrorMessage', 'Email is not valid');
    }
    if (!password) {
      this.setData('passwordErrorMessage', 'Password is to short');
    }
    return username && email && password;
  }

  get usernameIsValid() {
    const {username} = this.state;
    return username.length + 1 > 4;
  }
  get emailIsValid() {
    const emailRegexp = /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/;
    const {email} = this.state;
    return emailRegexp.test(email);
  }
  get passwordIsValid() {
    const {password} = this.state;
    return password.length + 1 > 4;
  }

  render() {
    const {
      username,
      email,
      password,
      usernameErrorMessage,
      passwordErrorMessage,
      emailErrorMessage,
    } = this.state;

    return (
      <ImageBackground style={style.background} source={Images.loginBg}>
        <SafeAreaView style={style.content}>
          <Icon
            style={style.logo}
            name="rocket"
            size={getPixel(100)}
            color={Colors.primary}
          />
          <View style={[style.form, style.form_inputs]}>
            <View style={style.form__input}>
              <TextInput
                label={usernameErrorMessage || 'Username'}
                autoCapitalize="none"
                autoCorrect={false}
                value={username}
                onChangeText={this.onChangeUsername}
                error={!!usernameErrorMessage}
              />
            </View>
            <View style={style.form__input}>
              <TextInput
                label={emailErrorMessage || 'Email'}
                keyboardType="email-address"
                autoCapitalize="none"
                autoCorrect={false}
                value={email}
                onChangeText={this.onChangeEmail}
                error={!!emailErrorMessage}
              />
            </View>
            <View style={style.form__input}>
              <TextInput
                label={passwordErrorMessage || 'Password'}
                autoCorrect={false}
                value={password}
                textContentType="password"
                secureTextEntry
                onChangeText={this.onChangePassword}
                error={!!passwordErrorMessage}
              />
            </View>
          </View>
          <View style={[style.form, style.form_btns]}>
            <Button
              style={[style.form__btn]}
              labelStyle={style.form__btnLabel}
              mode="contained"
              onPress={this.onPressSubmit}>
              Submit
            </Button>
            <Button
              style={[style.form__btn]}
              labelStyle={style.form__btnLabel}
              mode="contained"
              onPress={this.onPressSignIn}>
              Sign in
            </Button>
          </View>
        </SafeAreaView>
      </ImageBackground>
    );
  }
}

const mapStateToProps = (_state: IRootReducer) => ({});

const mapDispatchToProps = (_dispatch: Dispatch<AnyAction>) => ({
  setAuth: (value: any) => _dispatch(AuthActions.setAuth(value)),
});

export default connect(mapStateToProps, mapDispatchToProps)(RegisterContainer);
