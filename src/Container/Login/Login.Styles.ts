import { StyleSheet } from 'react-native';

import { Colors } from '../../../util/colors/index';
import { getPixel } from '../../../util/pixel';

export default StyleSheet.create({
  background: {
    width: '100%',
    height: '100%',
  },
  content: {
    flex: 1,
    alignItems: 'center',
    marginHorizontal: getPixel(20),
  },
  logo: {
    marginTop: getPixel(60),
    marginBottom: getPixel(120),
  },
  form: {
    width: '100%',
  },
  form_inputs: {
    marginBottom: getPixel(60),
  },
  form_btns: {
    display: 'flex',
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  form__input: {
    marginBottom: getPixel(20),
  },
  form__btnLabel: {
    marginVertical: getPixel(18),
    width: '100%',
  },
  form__btn: {
    width: '48%',
  },
});
