import { InferProps } from 'prop-types';
import React, { Component, Dispatch } from 'react';
import { SafeAreaView, Text, View } from 'react-native';
import Accordion from 'react-native-collapsible/Accordion';
import { Appbar } from 'react-native-paper';
import { connect } from 'react-redux';
import { AnyAction } from 'redux';

import { getPixel } from '../../../util/pixel';
import TaskContentComponent from '../../Component/TaskContent/TaskContent.Component';
import TaskHeaderComponent from '../../Component/TaskHeader/TaskHeader.Component';
import IDefaultProps from '../../Interface/IDefaultProps';
import { IRootReducer } from '../../Interface/IRootReducer';
import CurrentTasksStyles from './CurrentTasks.Styles';

type Props = {};

const style = CurrentTasksStyles;

class CurrentTasksContainer extends Component<
  InferProps<Props> & IDefaultProps
> {
  readonly state = {
    activeSections: [],
  };

  onClickSearch = () => {
    // console.tron.log('Searching');
  };

  _updateSections = (activeSections: any) => {
    this.setState({activeSections});
  };
  render() {
    const {activeSections} = this.state;
    return (
      <View>
        <Appbar.Header>
          <Appbar.Content title="Your Tasks" />
          <Appbar.Action
            size={getPixel(25)}
            icon="magnify"
            onPress={this.onClickSearch}
          />
        </Appbar.Header>
        <SafeAreaView style={style.content}>
          <Accordion
            sections={[
              {name: 'hey'},
              {name: 'hey'},
              {name: 'hey'},
              {name: 'hey'},
            ]}
            activeSections={activeSections}
            renderHeader={(section, index, isActive, sections) => (
              <TaskHeaderComponent
                section={section}
                index={index}
                isActive={isActive}
                sections={sections}
              />
            )}
            renderContent={(section, index, isActive, sections) => (
              <TaskContentComponent
                section={section}
                index={index}
                isActive={isActive}
                sections={sections}
              />
            )}
            onChange={this._updateSections}
          />
        </SafeAreaView>
      </View>
    );
  }
}

const mapStateToProps = (_state: IRootReducer) => ({});

const mapDispatchToProps = (_dispatch: Dispatch<AnyAction>) => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CurrentTasksContainer);
